<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-amap?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'amap_description' => 'The plugin is developed to manage baskets and subscibers. 
The baskets are created and allocated to a subscriber who can make their basket available if they cannot be there to pick it up on the arranged day.',
	'amap_nom' => 'CSA',
	'amap_slogan' => 'Baskets and subscribers management for CSA'
);
